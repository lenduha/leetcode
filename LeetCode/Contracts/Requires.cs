﻿namespace Contracts
{
    public static class Condition
    {
        public static InputValidator<T> Requires<T>(T param)
        {
            return new InputValidator<T>(param);
        }

        public static InputValidator<T> Requires<T>(T param, string paramName)
        {
            return new InputValidator<T>(param, paramName);
        }
    }
}
