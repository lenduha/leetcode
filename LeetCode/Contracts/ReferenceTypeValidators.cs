﻿using System;

namespace Contracts
{
    public static partial class Validators
    {
        public static InputValidator<T> NotNull<T>(this InputValidator<T> param) where T : class 
        {
            if (null == param.Value) throw new NullReferenceException();

            return param;
        }
    }
}
