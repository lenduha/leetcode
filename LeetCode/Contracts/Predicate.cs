﻿using System;

namespace Contracts
{
    public static class Predicate
    {
        public static InputValidator<T> Satisfies<T>(this InputValidator<T> input, Func<T, bool> predicate)
        {
            if (!predicate(input.Value)) throw new ArgumentException($"Given predicate has not been satisfied for {input.Value}");

            return input;
        }
    }
}
