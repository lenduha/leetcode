﻿using System;

namespace Contracts
{
    public static partial class Validators
    {
        #region Equal check

        public static InputValidator<int> IsEqualTo(this InputValidator<int> param, int value)
        {
            if (param.Value == value) ThrowEqualException(param.Value, value);

            return param;
        }

        public static InputValidator<long> IsEqualTo(this InputValidator<long> param, long value)
        {
            if (param.Value == value) ThrowEqualException(param.Value, value);

            return param;
        }

        #endregion Equal check

        #region InRange check

        public static InputValidator<int> IsInRange(this InputValidator<int> param, int min, int max)
        {
            if (param.Value < min || param.Value > max) ThrowRangeException(param.Value, min, max);

            return param;
        }

        public static InputValidator<double> IsInRange(this InputValidator<double> param, double min, double max)
        {
            if (param.Value < min || param.Value > max) ThrowRangeException(param.Value, min, max);

            return param;
        }

        public static InputValidator<long> IsInRange(this InputValidator<long> param, long min, long max)
        {
            if (param.Value < min || param.Value > max) ThrowRangeException(param.Value, min, max);

            return param;
        }

        #endregion InRange check

        #region GreaterThan check

        public static InputValidator<int> IsGreaterThan(this InputValidator<int> param, int value)
        {
            if (param.Value <= value) ThrowGreaterThanException(param.Value, value);

            return param;
        }

        public static InputValidator<double> IsGreaterThan(this InputValidator<double> param, double value)
        {
            if (param.Value <= value) ThrowGreaterThanException(param.Value, value);

            return param;
        }

        public static InputValidator<long> IsGreaterThan(this InputValidator<long> param, long value)
        {
            if (param.Value <= value) ThrowGreaterThanException(param.Value, value);

            return param;
        }

        #endregion GreaterThan check

        #region LessThan check

        public static InputValidator<int> IsLessThan(this InputValidator<int> param, int value)
        {
            if (param.Value >= value) ThrowLessThanException(param.Value, value);

            return param;
        }

        public static InputValidator<double> IsLessThan(this InputValidator<double> param, double value)
        {
            if (param.Value >= value) ThrowLessThanException(param.Value, value);

            return param;
        }

        public static InputValidator<long> IsLessThan(this InputValidator<long> param, long value)
        {
            if (param.Value >= value) ThrowLessThanException(param.Value, value);

            return param;
        }

        #endregion LessThan check

        #region Exceptions
        private static void ThrowRangeException<T>(T param, T min, T max)
        {
            throw new ArgumentOutOfRangeException($"{param} is not in the range [{min}, {max}]");
        }
        private static void ThrowGreaterThanException<T>(T param, T value)
        {
            throw new ArgumentException($"{param} is smaller than {value}");
        }

        private static void ThrowLessThanException<T>(T param, T value)
        {
            throw new ArgumentException($"{param} is bigger than {value}");
        }

        private static void ThrowEqualException<T>(T param, T value)
        {
            throw new ArgumentException($"{param} is not equal to {value}");
        }
        #endregion Exceptions
    }
}
