﻿using System;

namespace Contracts
{
    public sealed class InputValidator<T>
    {
        public InputValidator(T value)
        {
            Value = value;
            Name = nameof(T);
        }

        public InputValidator(T value, string name)
        {
            Value = value;
            Name = name;
        }

        internal T Value { get; }

        internal string Name { get; }
    }
}
