﻿using NUnit.Framework;
using FluentAssertions;
using LeetCode.Common;

namespace LeetCodeTests.Common
{
    [TestFixture]
    public class UtilitiesTests
    {
        [TestCase(0, 0, 0)]
        [TestCase(1, 0, 1)]
        [TestCase(0, 1, 1)]
        [TestCase(1, 1, 1)]
        [TestCase(5, 7, 1)]
        [TestCase(5, 15, 5)]
        [TestCase(15, 5, 5)]
        [TestCase(15, 17, 1)]
        [TestCase(6, 16, 2)]
        [TestCase(36, 6, 6)]
        [TestCase(12, 4, 4)]
        public static void GreatestCommonDivisor_GivenPositiveIntegers_CalculateGCD(int a, int b, int expectedResult)
        {
            Utilities.Gcd(a, b).Should().Be(expectedResult);
        }

        [TestCase(-12, 4, 4)]
        [TestCase(12, -4, 4)]
        [TestCase(-12, -4, 4)]
        [TestCase(0, -1, 1)]
        public static void GreatestCommonDivisor_GivenNegativeIntegers_CalculateGCD(int a, int b, int expectedResult)
        {
            Utilities.Gcd(a, b).Should().Be(expectedResult);
        }
    }
}
