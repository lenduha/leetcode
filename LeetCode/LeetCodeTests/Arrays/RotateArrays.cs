﻿using NUnit.Framework;
using FluentAssertions;

namespace LeetCodeTests.Arrays
{
    [TestFixture]
    public class RotateArrays
    {
        [Test, TestCaseSource(typeof(ArrayTestData), "EvenNumberOfItemsRightShift")]
        public static void ArrayRotationByMoving_EvenNumberOfItems_ShouldBeShiftedRight(int[] ar, int k, int[] expectedResult)
        {
            LeetCode.Arrays.RotateArray.RotateByMoving(ar, k);
            ar.Should().Equal(expectedResult);
        }

        [Test, TestCaseSource(typeof(ArrayTestData), "OddNumberOfItemsRightShift")]
        public static void ArrayRotationByMoving_OddNumberOfItems_ShouldBeShiftedRight(int[] ar, int k, int[] expectedResult)
        {
            LeetCode.Arrays.RotateArray.RotateByMoving(ar, k);
            ar.Should().Equal(expectedResult);
        }

        [Test, TestCaseSource(typeof(ArrayTestData), "EvenNumberOfItemsRightShift")]
        public static void ArrayRotationByMovingByGCD_EvenNumberOfItems_ShouldBeShiftedRight(int[] ar, int k, int[] expectedResult)
        {
            LeetCode.Arrays.RotateArray.RotateByMovingByGCD(ar, k);
            ar.Should().Equal(expectedResult);
        }

        [Test, TestCaseSource(typeof(ArrayTestData), "OddNumberOfItemsRightShift")]
        public static void ArrayRotationByMovingByGCD_OddNumberOfItems_ShouldBeShiftedRight(int[] ar, int k, int[] expectedResult)
        {
            LeetCode.Arrays.RotateArray.RotateByMovingByGCD(ar, k);
            ar.Should().Equal(expectedResult);
        }

        [Test, TestCaseSource(typeof(ArrayTestData), "EvenNumberOfItemsLeftShift")]
        public static void ArrayLeftRotationByReversing_EvenNumberOfItems_ShouldBeShiftedLeft(int[] ar, int k, int[] expectedResult)
        {
            LeetCode.Arrays.RotateArray.RotateLeftByReversing(ar, k);
            ar.Should().Equal(expectedResult);
        }

        [Test, TestCaseSource(typeof(ArrayTestData), "OddNumberOfItemsLeftShift")]
        public static void ArrayLeftRotationByReversing_OddNumberOfItems_ShouldBeShiftedLeft(int[] ar, int k, int[] expectedResult)
        {
            LeetCode.Arrays.RotateArray.RotateLeftByReversing(ar, k);
            ar.Should().Equal(expectedResult);
        }

        [Test, TestCaseSource(typeof(ArrayTestData), "EvenNumberOfItemsRightShift")]
        public static void ArrayRightRotationByReversing_EvenNumberOfItems_ShouldBeShiftedRight(int[] ar, int k, int[] expectedResult)
        {
            LeetCode.Arrays.RotateArray.RotateRightByReversing(ar, k);
            ar.Should().Equal(expectedResult);
        }

        [Test, TestCaseSource(typeof(ArrayTestData), "OddNumberOfItemsRightShift")]
        public static void ArrayRightRotationByReversing_OddNumberOfItems_ShouldBeShiftedRight(int[] ar, int k, int[] expectedResult)
        {
            LeetCode.Arrays.RotateArray.RotateRightByReversing(ar, k);
            ar.Should().Equal(expectedResult);
        }
    }
}
