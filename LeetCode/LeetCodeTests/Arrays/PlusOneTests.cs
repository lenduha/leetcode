﻿using NUnit.Framework;
using FluentAssertions;

namespace LeetCodeTests.Arrays
{
    [TestFixture]
    internal class PlusOneTests
    {
        [TestCase(new []{1, 2, 3}, new[] { 1, 2, 4 })]
        [TestCase(new []{ 4, 3, 2, 1 }, new[] { 4, 3, 2, 2 })]
        public static void PlusOne_NoCarry_ShouldReturnSameNumberOfItems(int[] array, int[] expectedResult)
        {
            LeetCode.Arrays.Manipulation.PlusOne(array).Should().BeEquivalentTo(expectedResult);
        }

        [TestCase(new[] { 1, 2, 9 }, new[] { 1, 3, 0 })]
        [TestCase(new[] { 4, 9, 9, 9 }, new[] { 5, 0, 0, 0 })]
        public static void PlusOne_CarryExists_ShouldReturnSameNumberOfItems(int[] array, int[] expectedResult)
        {
            LeetCode.Arrays.Manipulation.PlusOne(array).Should().BeEquivalentTo(expectedResult);
        }

        [TestCase(new[] { 9 }, new[] { 1, 0 })]
        [TestCase(new[] { 9, 9, 9, 9 }, new[] { 1, 0, 0, 0, 0 })]
        public static void PlusOne_CarryExists_ShouldReturnOneExtraItem(int[] array, int[] expectedResult)
        {
            LeetCode.Arrays.Manipulation.PlusOne(array).Should().BeEquivalentTo(expectedResult);
        }
    }
}
