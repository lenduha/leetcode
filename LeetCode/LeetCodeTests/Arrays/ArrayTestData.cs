﻿using System.Collections;
using NUnit.Framework;

namespace LeetCodeTests.Arrays
{
    public class ArrayTestData
    {
        public static IEnumerable EvenNumberOfItemsRightShift
        {
            get
            {
                yield return new TestCaseData(new int[] { }, 0, new int[] { });
                yield return new TestCaseData(new int[] { }, 1, new int[] { });
                yield return new TestCaseData(new int[] { 1 }, 1, new int[] { 1 });
                yield return new TestCaseData(new int[] {1, 2}, 0, new int[] {1, 2});
                yield return new TestCaseData(new int[] { 1, 2 }, 1, new int[] { 2, 1 });
                yield return new TestCaseData(new int[] { 1, 2 }, 2, new int[] { 1, 2 });
                yield return new TestCaseData(new int[] { 1, 2 }, 3, new int[] { 2, 1 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4 }, 0, new int[] { 1, 2, 3, 4 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4 }, 1, new int[] { 4, 1, 2, 3 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4 }, 2, new int[] { 3, 4, 1, 2 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4 }, 3, new int[] { 2, 3, 4, 1 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4 }, 4, new int[] { 1, 2, 3, 4 });
            }
        }

        public static IEnumerable OddNumberOfItemsRightShift
        {
            get
            {
                yield return new TestCaseData(new int[] {1}, 0, new int[] {1});
                yield return new TestCaseData(new int[] { 1 }, 1, new int[] { 1 });
                yield return new TestCaseData(new int[] { 1, 2, 3 }, 1, new int[] { 3, 1, 2 });
                yield return new TestCaseData(new int[] { 1, 2, 3 }, 2, new int[] { 2, 3, 1 });
                yield return new TestCaseData(new int[] { 1, 2, 3 }, 3, new int[] { 1, 2, 3 });
                yield return new TestCaseData(new int[] { 1, 2, 3 }, 4, new int[] { 3, 1, 2 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4, 5 }, 0, new int[] { 1, 2, 3, 4, 5 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4, 5 }, 1, new int[] { 5, 1, 2, 3, 4 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4, 5 }, 2, new int[] { 4, 5, 1, 2, 3 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4, 5 }, 3, new int[] { 3, 4, 5, 1, 2 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4, 5 }, 4, new int[] { 2, 3, 4, 5, 1 });
            }
        }

        public static IEnumerable EvenNumberOfItemsLeftShift
        {
            get
            {
                yield return new TestCaseData(new int[] { }, 0, new int[] { });
                yield return new TestCaseData(new int[] { }, 1, new int[] { });
                yield return new TestCaseData(new int[] { 1 }, 1, new int[] { 1 });
                yield return new TestCaseData(new int[] { 1, 2 }, 0, new int[] { 1, 2 });
                yield return new TestCaseData(new int[] { 1, 2 }, 1, new int[] { 2, 1 });
                yield return new TestCaseData(new int[] { 1, 2 }, 2, new int[] { 1, 2 });
                yield return new TestCaseData(new int[] { 1, 2 }, 3, new int[] { 2, 1 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4 }, 0, new int[] { 1, 2, 3, 4 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4 }, 1, new int[] { 2, 3, 4, 1 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4 }, 2, new int[] { 3, 4, 1, 2 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4 }, 3, new int[] { 4, 1, 2, 3 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4 }, 4, new int[] { 1, 2, 3, 4 });
            }
        }

        public static IEnumerable OddNumberOfItemsLeftShift
        {
            get
            {
                yield return new TestCaseData(new int[] { 1 }, 0, new int[] { 1 });
                yield return new TestCaseData(new int[] { 1 }, 1, new int[] { 1 });
                yield return new TestCaseData(new int[] { 1, 2, 3 }, 1, new int[] { 2, 3, 1 });
                yield return new TestCaseData(new int[] { 1, 2, 3 }, 2, new int[] { 3, 1, 2 });
                yield return new TestCaseData(new int[] { 1, 2, 3 }, 3, new int[] { 1, 2, 3 });
                yield return new TestCaseData(new int[] { 1, 2, 3 }, 4, new int[] { 2, 3, 1 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4, 5 }, 0, new int[] { 1, 2, 3, 4, 5 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4, 5 }, 1, new int[] { 2, 3, 4, 5, 1 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4, 5 }, 2, new int[] { 3, 4, 5, 1, 2 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4, 5 }, 3, new int[] { 4, 5, 1, 2, 3 });
                yield return new TestCaseData(new int[] { 1, 2, 3, 4, 5 }, 4, new int[] { 5, 1, 2, 3, 4 });
            }
        }
    }
}
