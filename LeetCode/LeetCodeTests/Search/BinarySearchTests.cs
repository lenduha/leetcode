﻿using NUnit.Framework;
using FluentAssertions;

namespace LeetCodeTests.Search
{
    [TestFixture]
    public class BinarySearchTests
    {
        private int[] _input = null;

        [OneTimeSetUp]
        public void Init()
        {
            _input = new[] { 1, 3, 5, 6 };
        }

        [TestCase(1, 0)]
        [TestCase(3, 1)]
        [TestCase(5, 2)]
        [TestCase(6, 3)]
        public void BinarySearch_ExistingIntegersInArray_ShouldReturnIndex(int target, int expectedIndex)
        {
            LeetCode.Search.Search.Binary(_input, target).Should().Be(expectedIndex);
        }

        [TestCase(-1)]
        [TestCase(0)]
        [TestCase(2)]
        [TestCase(4)]
        [TestCase(7)]
        public void BinarySearch_NonExistingIntegersInArray_ShouldReturnIndex(int target)
        {
            LeetCode.Search.Search.Binary(_input, target).Should().Be(-1);
        }
    }
}
