﻿using FluentAssertions;
using LeetCode.Trees;
using NUnit.Framework;

namespace LeetCodeTests.Trees
{
    [TestFixture]
    public class Serialization
    {
        [Test]
        public void CheckSerialization()
        {
            var tree = Helpers.CreateTestTree();

            var result = Traversal.SerializeToStringPreOrder(tree);

            result.Should().Be("9,7,3,#,#,2,1,#,#,#,10,4,#,#,8,#,6,#,#");
        }

        [Test]
        public void CheckDeserialization()
        {
            var serializedTree = "9,7,3,#,#,2,1,#,#,#,10,4,#,#,8,#,6,#,#";

            var result = Traversal.DeserializeFromStringPreOrder(serializedTree);
            //TODO add assertion
        }
    }
}
