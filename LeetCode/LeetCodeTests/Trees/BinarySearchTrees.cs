﻿using System.Collections.Generic;
using System.Linq;
using LeetCode.Trees;
using LeetCode.Trees.Utilities;
using NUnit.Framework;
using FluentAssertions;

namespace LeetCodeTests.Trees
{

    [TestFixture]
    public class BinarySearchTrees
    {
        [Test]
        public void BinarySearchTree_Create_New_ByReturnsRoorImpl()
        {
            var root = Helpers.CreateBST();

            Traversal.InOrderIterative(root).Should().Equal(new List<int>(){1,2,3,4,5,6,7});
        }

        [Test]
        public void BinarySearchTree_Create_New_ByRefImpl()
        {
            var values = new List<int>() { 5, 4, 2, 7, 1, 3, 6 };
            var root = TreeNode.Null;

            foreach (var value in values)
            {
                BinarySearchTree.InsertRecursiveByRef(ref root, new TreeNode(value));
            }

            Traversal.InOrderIterative(root).Should().Equal(new List<int>() { 1, 2, 3, 4, 5, 6, 7 });
        }
    }
}
