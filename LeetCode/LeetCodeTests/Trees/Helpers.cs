﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LeetCode.Trees;
using LeetCode.Trees.Utilities;

namespace LeetCodeTests.Trees
{
    internal static class Helpers
    {
        internal static TreeNode CreateTestTree()
        {
            var n1 = new TreeNode(1);
            var n2 = new TreeNode(2);
            var n3 = new TreeNode(3);
            var n7 = new TreeNode(7);
            var n9 = new TreeNode(9);
            var n10 = new TreeNode(10);
            var n4 = new TreeNode(4);
            var n8 = new TreeNode(8);
            var n6 = new TreeNode(6);

            n2.left = n1;
            n7.left = n3;
            n7.right = n2;

            n8.right = n6;
            n10.left = n4;
            n10.right = n8;

            n9.left = n7;
            n9.right = n10;

            return n9;
        }

        internal static TreeNode CreateFullTree()
        {
            var n1 = new TreeNode(1);
            var n2 = new TreeNode(2);
            var n3 = new TreeNode(3);
            var n7 = new TreeNode(7);
            var n9 = new TreeNode(9);
            var n10 = new TreeNode(10);
            var n4 = new TreeNode(4);
            var n8 = new TreeNode(8);
            var n6 = new TreeNode(6);

            n4.left = n8;
            n4.right = n6;

            n7.left = n9;
            n7.right = n10;

            n1.left = n4;
            n1.right = n7;

            return n1;
        }

        internal static TreeNode CreateBST()
        {
            var values = new List<int>() { 5, 4, 2, 7, 1, 3, 6 };
            var root = TreeNode.Null;

            foreach (var value in values)
            {
                root = BinarySearchTree.InsertRecursiveReturnsRoot(root, new TreeNode(value));
            }

            return root;
        }
    }
}
