﻿using NUnit.Framework;
using FluentAssertions;

namespace LeetCodeTests.Trees
{
    [TestFixture]
    public class IsValidBST
    {
        [Test]
        public void Validate_ByInorder_Valid_BST()
        {
            var bst = Helpers.CreateBST();
            LeetCode.Trees.IsValidBST.ByInorder(bst).Should().BeTrue();
        }

        [Test]
        public void Validate_ByInorder_Invalid_BST_BinaryTree()
        {
            var notBst = Helpers.CreateTestTree();
            LeetCode.Trees.IsValidBST.ByInorder(notBst).Should().BeFalse();
        }

        [Test]
        public void Validate_ByInorder_Invalid_BST_FullBinaryTree()
        {
            var notBst = Helpers.CreateFullTree();
            LeetCode.Trees.IsValidBST.ByInorder(notBst).Should().BeFalse();
        }
    }
}
