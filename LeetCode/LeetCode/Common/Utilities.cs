﻿using System;
using Contracts;

namespace LeetCode.Common
{
    public static class Utilities
    {
        public static void Swap<T>(ref T a, ref T b)
        {
            var temp = b;
            b = a;
            a = temp;
        }

        public static void Reverse<T>(T[] input, int left, int right)
        {
            Condition.Requires(left).IsGreaterThan(-1);
            Condition.Requires(right).IsGreaterThan(0);

            var start = left;
            var end = right-1;

            while (start <= end)
            {
                Swap(ref input[start++], ref input[end--]);
            }
        }

        /// <summary>
        /// Finds the Greatest Common Divisor of the given integers 
        /// </summary> 
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <remarks>Parameters a and b can be either positive or negative</remarks>
        /// <returns>GCD of a and b</returns>
        public static int Gcd(int a, int b)
        {
            a = Math.Abs(a);
            b = Math.Abs(b);

            if (a < b) Swap(ref a, ref b);

            return GcdOfPositives(a, b);
        }

        private static int GcdOfPositives(int a, int b)
        {
            return b == 0
            ? a
            : GcdOfPositives(b, a % b);
        }
    }
}
