﻿using Contracts;
using LeetCode.Common;

namespace LeetCode.Arrays
{
    public static class RotateArray
    {
        public static void RotateByMoving(int[] nums, int k)
        {
            var numberCount = nums.Length;
            var count = 0;

            for (var shift = 0; count < numberCount; shift++)
            {
                var current = shift;
                var prev = nums[shift];

                do
                {
                    var next = (current + k) % numberCount;
                    var temp = nums[next];
                    nums[next] = prev;
                    prev = temp;
                    current = next;
                    count++;

                } while (current != shift);
            }
        }

        public static void RotateByMovingByGCD(int[] nums, int k)
        {
            Condition.Requires(k).IsGreaterThan(-1);
            Condition.Requires(nums).NotNull();

            var numberCount = nums.Length;

            if (numberCount == 0) return;

            var gcd = Utilities.Gcd(numberCount, k);

            for (var iteration = 0; iteration < gcd; ++iteration)
            {
                var current = iteration;
                var prev = nums[current];

                do
                {
                    var next = (current + k) % numberCount;
                    var temp = nums[next];
                    nums[next] = prev;
                    prev = temp;
                    current = next;

                } while (current != iteration);
            }
        }

        public static void RotateRightByReversing<T>(T[] nums, int k)
        {
            Condition.Requires(k).IsGreaterThan(-1);

            var numberCount = nums.Length;
            if (numberCount == 0) return;
            
            //shifting right for k times
            //means shifting left for (n-k) times
            var line = k % numberCount;
            k = numberCount - line;

            RotateArrayByReversing(nums, k);
        }

        public static void RotateLeftByReversing<T>(T[] nums, int k)
        {
            Condition.Requires(k).IsGreaterThan(-1);
            var numberCount = nums.Length;
            if (numberCount == 0) return;

            //Handle loop on the array
            //when number of rotation is bigger than array size
            k = k > numberCount ? k % numberCount : k;

            RotateArrayByReversing(nums, k);
        }

        private static void RotateArrayByReversing<T>(T[] nums, int k)
        {
            Condition.Requires(nums).NotNull();
            Condition.Requires(k).IsInRange(0, nums.Length);
            var numberCount = nums.Length;

            if (k == 0 || numberCount == k || numberCount == 0) return;

            Utilities.Reverse(nums, 0, k);
            Utilities.Reverse(nums, k, numberCount);
            Utilities.Reverse(nums, 0, numberCount);
        }
    }
}
