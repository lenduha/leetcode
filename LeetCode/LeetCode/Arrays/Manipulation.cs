﻿using System;
using Contracts;

namespace LeetCode.Arrays
{
    public static class Manipulation
    {
        public static int[] PlusOne(int[] digits)
        {
            Condition.Requires(digits).NotNull();

            var carried = 1;
            var len = digits.Length;

            for (var i = len - 1; i >= 0; --i)
            {
                var sum = (digits[i] + carried);

                digits[i] = sum % 10;
                carried = sum / 10;
            }

            return (carried == 1) ? CopyArrayAndPrepend(digits, carried) : digits;
        }

        private static int[] CopyArrayAndPrepend(int[] array, int item)
        {
            var newArray = new int[array.Length + 1];
            Array.Copy(array, 0, newArray, 1, array.Length);
            newArray[0] = item;

            return newArray;
        }
    }
}
