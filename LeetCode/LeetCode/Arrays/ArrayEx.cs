﻿using System;
using System.Text;

namespace LeetCode.Arrays
{
    public static class ArrayEx
    {
        public static void Print<T>(this T[] array)
        {
            Console.WriteLine(array.ToString());
        }

        public static string ToString<T>(this T[] array)
        {
            var builder = new StringBuilder();

            foreach (var item in array)
            {
                builder.Append($"{item} ");
            }

            return builder.ToString();
        }
    }
}
