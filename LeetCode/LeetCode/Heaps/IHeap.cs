﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeetCode.Heaps
{
    interface IHeap<T>
    {
        /// <summary>
        /// Pushes a new key to the heap
        /// </summary>
        /// <param name="key"></param>
        void Insert(T key);
        /// <summary>
        /// Pop the root and push the given key
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Root of the heap (min/max)</returns>
        T Replace(T key);
    }
}
