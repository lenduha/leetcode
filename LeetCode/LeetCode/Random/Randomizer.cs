﻿using System;

namespace LeetCode.Random
{
    public static class Randomizer
    {
        public static void Shuffle(int[] input)
        {
            var itemNumber = input.Length;
            var rand = new System.Random(DateTime.Now.Millisecond);

            for (var i = 0; i < itemNumber-1; ++i)
            {
                Common.Utilities.Swap(ref input[i], ref input[rand.Next(i+1, itemNumber)]);
            }
        }
    }
}
