﻿using System.Collections.Generic;
using Contracts;

namespace LeetCode.Search
{
    public static class Search
    {
        public static int Binary<T>(T[] array, T target, Comparer<T> comparer = null)
        {
            Condition.Requires(array).NotNull();
            var comparerInUse = comparer ?? Comparer<T>.Default;

            return BinarySearch(array, 0, array.Length - 1, target);

            //Local method for actual Binary Search 
            int BinarySearch(T[] nums, int left, int right, T givenTarget)
            {
                var len = right - left + 1;

                if (len <= 0) return -1;

                var middle = (right + left) / 2;
                var comparison = comparerInUse.Compare(target, array[middle]);

                if (comparison < 0) return BinarySearch(array, left, middle - 1, target);
                if (comparison > 0) return BinarySearch(array, middle + 1, right, target);
                return middle;
            }
        }

    }
}
