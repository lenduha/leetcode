﻿using System;
using System.Collections.Generic;
using Contracts;
using LeetCode.Common;

namespace LeetCode.Sorting
{
    public static class Selection
    {
        public static void Sort<T>(T[] input)
        {
            Condition.Requires(input).NotNull();
            Sort(input, Comparer<T>.Default);
        }

        public static void Sort<T>(T[] input, IComparer<T> comparer)
        {
            var itemNumber = input.Length;

            for (var i = 0; i < itemNumber-1; ++i)
            {
                var min = i;

                for (var j = i+1; j < itemNumber; ++j)
                {
                    if (comparer.Compare(input[j], input[min]) < 0)
                    {
                        min = j;
                    }
                }
                if (i != min) Utilities.Swap(ref input[i], ref input[min]);
            }
        }
    }
}
