﻿using System.Collections.Generic;
using Contracts;

namespace LeetCode.Sorting
{
    public static class Bubble
    {
        public static void Sort<T>(T[] input)
        {
            Condition.Requires(input).NotNull();
            Sort(input, Comparer<T>.Default);
        }

        public static void Sort<T>(T[] input, IComparer<T> comparer)
        {
            var itemNumber = input.Length;
            var swapped = true;

            while (swapped)
            {
                swapped = false;

                for (var i = 0; i < itemNumber-1; i++)
                {
                    if (comparer.Compare(input[i],input[i + 1]) > 0)
                    {
                        Common.Utilities.Swap(ref input[i], ref input[i+1]);
                        swapped = true;
                    }
                }
            }
        }
    }
}
