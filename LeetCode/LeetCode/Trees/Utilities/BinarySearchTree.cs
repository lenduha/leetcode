﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode.Trees.Utilities
{
    public static class BinarySearchTree
    {
        public static TreeNode InsertRecursiveReturnsRoot(TreeNode root, TreeNode node)
        {
            if (root == null)
            {
                root = node;
                return root;
            }

            if (node.val < root.val) root.left = InsertRecursiveReturnsRoot(root.left, node);
            if (node.val >= root.val) root.right = InsertRecursiveReturnsRoot(root.right, node);

            return root;
        }

        public static void InsertRecursiveByRef(ref TreeNode root, TreeNode node)
        {
            if (root == null)
            {
                root = node;
                return;
            }

            if (node.val < root.val) InsertRecursiveByRef(ref root.left, node);
            if (node.val >= root.val) InsertRecursiveByRef(ref root.right, node);
        }
    }
}
