﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode.Trees
{
    public static class Traversal
    {
        public static IEnumerable<int> PreOrderIterative(TreeNode root)
        {
            if (root == null) yield break;

            var stack = new Stack<TreeNode>();
            var node = root;

            while (stack.Count > 0 || node != null)
            {
                while (node != null)
                {
                    stack.Push(node);
                    yield return node.val;
                    node = node.left;
                }

                node = stack.Pop();

                node = node.right;
            }
        }

        public static IEnumerable<int> InOrderIterative(TreeNode root)
        {
            if (root == null) yield break;

            var stack = new Stack<TreeNode>();
            stack.Push(root);
            var node = root.left;


            while (stack.Count > 0 || node != null)
            {
                while (node != null)
                {
                    stack.Push(node);
                    node = node.left;
                }

                node = stack.Pop();
                yield return node.val;

                node = node.right;
            }
        }

        public static IEnumerable<string> SerializePreOrder(TreeNode root)
        {
            var result = new List<string>();

            if (root == null) return result;

            var stack = new Stack<TreeNode>();
            var node = root;


            while (stack.Count > 0 || node != null)
            {
                while (node != null)
                {
                    stack.Push(node);
                    result.Add(node.val.ToString());
                    node = node.left;
                }
                result.Add("#");

                node = stack.Pop();

                node = node.right;
            }
            result.Add("#");

            return result;
        }

        public static string SerializeToStringPreOrder(TreeNode root)
        {
            if (root == null) return string.Empty;

            var stack = new Stack<TreeNode>();
            var node = root;

            var result = new StringBuilder();

            while (stack.Count > 0 || node != null)
            {
                while (node != null)
                {
                    stack.Push(node);
                    result.Append(node.val.ToString() + ",");
                    node = node.left;
                }
                result.Append("#,");

                node = stack.Pop();

                node = node.right;
            }
            result.Append("#");

            return result.ToString();
        }

        public static TreeNode DeserializeFromStringPreOrder(string serial)
        {
            if (string.IsNullOrEmpty(serial)) return null;

            var nodes = serial.Split(',');
            var numberOfNodes = nodes.Count();
            var stack = new Stack<TreeNode>();
            var node = new TreeNode(Convert.ToInt32(nodes[0]));
            var root = node;
            stack.Push(node);

            for (var i = 1; i < numberOfNodes-1;)
            {
                while (!nodes[i].Equals("#"))
                {
                    node.left = new TreeNode(Convert.ToInt32(nodes[i++]));
                    node = node.left;
                    stack.Push(node);
                }
                ++i;

                if (stack.Count > 0)
                    node = stack.Pop();

                if (!nodes[i].Equals("#"))
                {
                    node.right = new TreeNode(Convert.ToInt32(nodes[i++]));
                    node = node.right;
                    stack.Push(node);
                }
            }

            return root;
        }
    }
}
