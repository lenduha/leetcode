﻿using System;
using System.Collections.Generic;
using System.Text;
using LeetCode.Trees;

namespace LeetCode.Trees.Utilities
{
    public static class BinaryTree
    {
        public static void SwapData(TreeNode parent, TreeNode child)
        {
            var temp = child.val;
            child.val = parent.val;
            parent.val = temp;
        }

        public static TreeNode SiftUpRecursive(TreeNode root, LinkedList<int> path)
        {
            if (path.First == null) return root;

            TreeNode node = null;

            switch (path.First.Value)
            {
                case 0:
                    path.RemoveFirst();
                    node = SiftUpRecursive(root.left, path);
                    break;

                case 1:
                    path.RemoveFirst();
                    node = SiftUpRecursive(root.left, path);
                    break;

                default:
                    throw new ArgumentException();
            }
            
            if (node != null && node.val > root.val ) SwapData(root, node);

            return root;
        }

        public static TreeNode HeapifyIterative(TreeNode root, LinkedList<int> path)
        {
            if (root == null) return null;

            var realRoot = root;
            var visitedNodes = new Stack<TreeNode>();
            visitedNodes.Push(root);

            while (path.Count > 0)
            {
                switch (path.First.Value)
                {
                    case 0:
                        MoveNextNode(root.left);
                        break;

                    case 1:
                        MoveNextNode(root.right);
                        break;

                    default:
                        throw new ArgumentException();
                }
            }

            IterateVisitedNodes();

            return realRoot;

            /* Helper inner functions */
            #region Inner.Methods
            void MoveNextNode(TreeNode node)
            {
                path.RemoveFirst();
                root = node;
                visitedNodes.Push(root);
            }

            void IterateVisitedNodes()
            {
                while (visitedNodes.Count > 1)
                {
                    var child = visitedNodes.Pop();
                    var parent = visitedNodes.Pop();

                    if (child.val <= parent.val) break;

                    SwapData(parent, child);
                    visitedNodes.Push(parent);
                }
            }
            #endregion Inner.Methods
        }
    }
}
