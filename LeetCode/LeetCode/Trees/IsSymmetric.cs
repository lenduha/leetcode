﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode.Trees
{
    //TODO Write tests
    //TODO Implement iteratively
    class IsSymmetric
    {
        private static bool Recursive(TreeNode q, TreeNode p)
        {
            return IsMirror(q, p);
        }

        private static bool IsMirror(TreeNode left, TreeNode right)
        {
            if (left == null && right == null) return true;
            if (left == null || right == null) return false;

            return (left.val == right.val) &&
                   IsMirror(left.left, right.right) &&
                   IsMirror(left.right, right.left);
        }
    }
}
