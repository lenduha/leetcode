﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode.Trees
{
    //TODO Implement iterative version
    public static class IsSameTree
    {
        public static bool Recursive(TreeNode p, TreeNode q)
        {
            if (p == null)
            {
                if (q != null) return false;
                return true;
            }

            if (q == null)
            {
                if (p != null) return false;
            }

            return (p.val == q.val) && Recursive(p.left, q.left) && Recursive(p.right, q.right);
        }
    }
}
