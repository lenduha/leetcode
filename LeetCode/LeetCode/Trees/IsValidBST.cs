﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode.Trees
{
    public class IsValidBST
    {
        public static bool ByInorder(TreeNode root)
        {
            if (root == null) return true;

            var stack = new Stack<TreeNode>();
            var node = root;
            var prev = TreeNode.Null;

            while (stack.Count > 0 || node != null)
            {
                while (node != null)
                {
                    stack.Push(node);
                    node = node.left;
                }

                node = stack.Pop();

                if (prev != null && prev.val >= node.val) return false;
                prev = node;
                node = node.right;
            }

            return true;
        }
    }
}
