﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts;
using LeetCode.Arrays;
using LeetCode.Trees;

namespace LeetCodeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var ar1 = new int[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            var ar2 = (int[]) null;

            Condition.Requires(ar1)
                .NotNull()
                .Satisfies(a => a.Length > 0);

            for (var i = 0; i <= ar1.Length; ++i)
            {
                var x = (int[]) ar1.Clone();
                RotateArray.RotateRightByReversing(x, i);
                Print(x);
            }

            //RotateArray(ar1, 3);
            //var tree = CreateFullTree();
            //var t1 = LevelOrder(tree);
            //LeetCode.Trees.Utilities.BinaryTree.HeapifyIterative(tree, new LinkedList<int>(new List<int>() { 0, 1, 1, 0 }));
            //var t2 = LevelOrder(tree);

            //LeetCode.Random.Randomizer.Shuffle(ar1);
            ////var ll = new List<int>(ar1);
            ////ll.Sort((x, y) =>
            ////    {

            ////        if (x < y) return 1;
            ////        if (x > y) return -1;
            ////        return 0;
            ////    });

            //LeetCode.Sorting.Selection.Sort(ar1, Comparer<int>.Create(
            //    (x, y) =>
            //    {

            //        if (x < y) return 1;
            //        if (x > y) return -1;
            //        return 0;
            //    }));
        }

        internal static TreeNode CreateFullTree()
        {
            var n1 = new TreeNode(1);
            var n2 = new TreeNode(2);
            var n3 = new TreeNode(3);
            var n5 = new TreeNode(5);
            var n7 = new TreeNode(7);
            var n9 = new TreeNode(9);
            var n10 = new TreeNode(10);
            var n11 = new TreeNode(11);
            var n12 = new TreeNode(12);
            var n4 = new TreeNode(4);
            var n8 = new TreeNode(8);
            var n6 = new TreeNode(6);

            n11.left = n6;
            n11.right = n7;
            n6.left = n8;
            n6.right = n4;
            n7.left = n9;
            n7.right = n10;
            n4.left = n3;
            n4.right = n2;
            n2.left = n5;

            return n11;
        }

        private static IList<IList<int>> LevelOrder(TreeNode root)
        {
            var result = new List<IList<int>>();

            if (root == null) return result;

            var queue = new Queue<TreeNode>();
            queue.Enqueue(root);
            queue.Enqueue(null);
            var isNullBefore = false;

            var list = new List<int>();
            while (queue.Count > 0)
            {
                var next = queue.Dequeue();
                if (next == null)
                {
                    if (isNullBefore) return result;
                    isNullBefore = true;
                    queue.Enqueue(null);
                    result.Add(list);
                    list = new List<int>();
                    continue;
                }

                isNullBefore = false;
                list.Add(next.val);
                if (next.left != null) queue.Enqueue(next.left);
                if (next.right != null) queue.Enqueue(next.right);
            }

            return result;
        }

        //private static IList<IList<int>> LevelOrder1(TreeNode root)
        //{
        //    var result = new List<IList<int>>();

        //    if (root == null) return result;

        //    var buffer = new LinkedList<TreeNode>();
        //    buffer.AddLast(root);


        //    while (buffer.Count > 0)
        //    {
        //        var level = new List<int>();

        //        foreach (var next in buffer)
        //        {
        //            level.Add(next.val);
        //            if (next.left != null) buffer.AddLast(next.left);
        //            if (next.right != null) buffer.AddLast(next.right);
        //        }

        //        buffer.Clear();

        //    }

        //    return result;
        //}

        private class DescengingInteger : Comparer<int>
        {
            public override int Compare(int x, int y)
            {
                if (x < y) return 1;
                if (x > y) return -1;
                return 0;
            }
        }

        private static void Print<T>(IEnumerable<T> items)
        {
            foreach (var it in items)
            {
                Console.Write(it + ", ");
            }
            Console.WriteLine();
        }
    }
}
